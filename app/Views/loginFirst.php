<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

    <!-- fontawesome cdn -->
    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://kit.fontawesome.com/8b0fcf1687.js" crossorigin="anonymous"></script>
    <!-- Leafletjs -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <!-- kml plugin -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet-ajax/2.1.0/leaflet.ajax.min.js"></script>
    <script src='//api.tiles.mapbox.com/mapbox.js/plugins/leaflet-omnivore/v0.3.1/leaflet-omnivore.min.js'></script>

    <!-- kml plugin -->
    <style>
        #baramap {
            height: 90vh;
        }
    </style>
    <!-- Leafletjs -->
    <title>Simudik2</title>
</head>

<body>
    <!-- Container -->
    <div class="container-fluid">
        <div class="row bg-dark text-light">
            <div class="col-lg-4 mx-auto text-center">
                <h3>SIMUDIK 2.0 ALPHA</h3>
            </div>
        </div>
        <div class="row bg-dark">
            <div class="col">
                <!-- navbar -->
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                    <a class="navbar-brand" href="#">Simudik 2.0</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <div class="mr-auto">&nbsp;</div>
                        <!-- <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
                            Dropdown
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled">Disabled</a>
                    </li>
                </ul> -->
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2 form-control-sm" type="text" placeholder="Username" aria-label="Username">
                            <input class="form-control mr-sm-2 form-control-sm" type="password" placeholder="Password" aria-label="Password">
                            <button class="btn btn-sm btn-success my-2 my-sm-0" type="submit">Login</button>
                        </form>
                    </div>
                </nav>
                <!-- navbar -->
            </div>
        </div>
        <div class="row" style="min-height:90vh;">
            <div class="col-lg-12 my-auto bg-secondary">
                <!-- Peta Banjarnegara -->
                <div id="baramap"></div>

            </div>
        </div>
        <?php echo view('template/js'); ?>
        <script>
            var map = L.map('baramap').setView([-7.35, 109.69], 12);
            mapLink = '<a href="http://www.esri.com/">Esri</a>';
            wholink = 'i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community';
            L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}?access_token=pk.eyJ1IjoibnVncm9ob3JlZGJ1ZmYiLCJhIjoiY2ptNGJqbHV5MGwwZjNwa2NkY2VrNTV2dSJ9.GwtW5j2D5EjiAUXNOojy9A', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                attribution: '&copy; ' + mapLink,
                //  + ', ' + wholink,
                maxZoom: 17,
                minZoom: 11,
                id: 'mapbox.streets'
            }).addTo(map);

            // Script Omivore to display kml data
            var runLayer = omnivore.kml('Banjarnegarakab.kml')
                .on('ready', function() {
                    map.fitBounds(runLayer.getBounds());

                    //popup
                    // href = 'datasources/Administrator/sekolahTampil/kec/1'
                    runLayer.eachLayer(function(layer) {
                        let kec = layer.feature.properties.name;
                        // let link = '<a href="javascript:void(0)" onclick=showSD("' + kec + '")>' + kec + '</a>';
                        //layer.bindPopup(layer.feature.properties.name);
                        // layer.bindPopup(link);
                    });

                })
                .addTo(map);
        </script>
    </div>
    <!-- Container -->
</body>

</html>