<div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
        <li><a><i class="fas fa-home"></i>Sekolah <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="<?= base_url(); ?>">Data Sekolah</a></li>
                <li><a href="<?= base_url(); ?>">Prestasi</a></li>
                <li><a href="<?= base_url(); ?>">Akreditasi</a></li>
                <li><a href="<?= base_url(); ?>">Regrouping</a></li>
                <li><a href="<?= base_url(); ?>">Status Tanah</a></li>
            </ul>
        </li>
        <li><a><i class="fas fa-database"></i>Data Penunjang <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="<?= base_url(); ?>">Guru</a></li>
                <li><a href="<?= base_url(); ?>">Pengawas</a></li>
                <li><a href="<?= base_url(); ?>">Rekanan</a></li>

            </ul>
        </li>
        <li><a><i class="fas fa-clipboard-list"></i>Rekap <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="<?= base_url(); ?>">Rawan Bencana</a></li>
                <li><a href="<?= base_url(); ?>">Perpustakaan</a></li>
            </ul>
        </li>
        <li><a><i class="fas fa-file-contract"></i>Barjas <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="<?= base_url(); ?>">Arsip Barjas</a></li>
            </ul>
        </li>

        <li><a><i class="fas fa-chart-area"></i>Statistik <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="<?= base_url(); ?>"> Sekolah </a></li>
                <li><a href="<?= base_url(); ?>"> Ruang </a></li>
                <li><a href="<?= base_url(); ?>"> Mebeler </a></li>
                <li><a href="<?= base_url(); ?>"> Alat Peraga </a></li>
                <li><a href="<?= base_url(); ?>"> Bangunan Pendukung </a></li>
                <li><a href="<?= base_url(); ?>"> Usulan </a></li>
            </ul>
        </li>

        <li><a><i class="fas fa-user-graduate"></i>SARPRAS BOS <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="<?= base_url(); ?>"> Rekap </a></li>
                <li><a href="<?= base_url(); ?>"> Pengadaan </a></li>
                <li><a href="<?= base_url(); ?>"> Perawatan </a></li>
            </ul>
        </li>

        <li><a><i class="fas fa-gift"></i>BANSOS <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="<?= base_url(); ?>"> Rekap </a></li>
                <li><a href="<?= base_url(); ?>"> PIP </a></li>
                <li><a href="<?= base_url(); ?>"> BSM </a></li>
            </ul>
        </li>

        <li><a><i class="fas fa-hard-hat"></i>PENGAWASAN <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="<?= base_url(); ?>"> Data Pengawasan </a></li>
            </ul>
        </li>
    </ul>
</div>
<div class="menu_section">
    <h3>SESSION</h3>
    <ul class="nav side-menu">
        <li><a><i class="fas fa-print"></i>Cetak</a></li>
        <li><a><i class="fas fa-sign-out-alt"></i>Logout</a></li>
    </ul>
</div>
<!--div class="menu_section">
    <h3>Live On</h3>
    <ul class="nav side-menu">
        <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="e_commerce.html">E-commerce</a></li>
                <li><a href="projects.html">Projects</a></li>
                <li><a href="project_detail.html">Project Detail</a></li>
                <li><a href="contacts.html">Contacts</a></li>
                <li><a href="profile.html">Profile</a></li>
            </ul>
        </li>
        <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="page_403.html">403 Error</a></li>
                <li><a href="page_404.html">404 Error</a></li>
                <li><a href="page_500.html">500 Error</a></li>
                <li><a href="plain_page.html">Plain Page</a></li>
                <li><a href="login.html">Login Page</a></li>
                <li><a href="pricing_tables.html">Pricing Tables</a></li>
            </ul>
        </li>
        <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="#level1_1">Level One</a>
                <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li class="sub_menu"><a href="level2.html">Level Two</a>
                        </li>
                        <li><a href="#level2_1">Level Two</a>
                        </li>
                        <li><a href="#level2_2">Level Two</a>
                        </li>
                    </ul>
                </li>
                <li><a href="#level1_2">Level One</a>
                </li>
            </ul>
        </li>
        <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
    </ul>
</div-->