<?php

namespace App\Controllers;

class Kabupaten extends BaseController
{
    public function index()
    {
        // helper("filesystem");
        $data = [
            'title' => "SIMUDIK 2.0"
        ];

        return view('kabupaten/index', $data);
    }
}
