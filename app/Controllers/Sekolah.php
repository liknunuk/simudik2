<?php

namespace App\Controllers;

class Sekolah extends BaseController
{
    public function index()
    {
        // helper("filesystem");
        $data = [
            'title' => "SIMUDIK 2.0"
        ];

        return view('sekolah/index', $data);
    }
}
