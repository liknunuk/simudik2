<?php

namespace App\Controllers;

class Kecamatan extends BaseController
{
    public function index()
    {
        // helper("filesystem");
        $data = [
            'title' => "SIMUDIK 2.0"
        ];

        return view('kecamatan/index', $data);
    }
}
