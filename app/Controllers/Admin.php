<?php

namespace App\Controllers;

class Admin extends BaseController
{
    public function index()
    {
        // helper("filesystem");
        $data = [
            'title' => "SIMUDIK 2.0"
        ];

        return view('admin/index', $data);
    }
}
